# Java wrapper

---

A command-line Java application that uses a WEKA machine learning model to predict the class of new, unknown instances.

---

## Clone the repository

---

Use these steps to clone this repository.

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Copy the clone command (either the SSH format or the HTTPS).
3. Using the command-line, go to the directory where you want the repository.
4. Paste the clone command in the command-line and press enter.
5. Open the directory to see the repository’s files.

---

## Usage

---

How use the command-line Java application:

1. Using the command-line, go to the repository directory
2. Type the following command:

**java -jar java_wrapper-1.0-SNAPSHOT-all.jar <OPTION> "<ARGUMENT>"**

- *option:*
	* -h
	* -f
	* -i

- *argument*
	* -h: no arguments required
	* -f: path to the .arff file
	* -i: a single instance


**requirements for the file argument:**

- f:
	1. The file has to be a .arff file.
	2. The data has to be log2 transformed and scaled.
	3. There can be no missing values.
	4. Must contain ten attributes and one class attribute.
	
for more information on how I did the steps above, read the log file in this repository: [Machine Learning project Log.rmd](https://bitbucket.org/darkpants/thema9/src/master/Log.Rmd)

**requirements for the single instance argument:**

- i:
	1. comma seperated attribute values.
	2. needs ten attributes. Missing values allowed, comma is needed. (i.e.: 1,,3)
	3. Missing values will default to 0. (i.e.: 1,0,3)

---

## Acknowledgments

---

* Special thanks to Michiel Noback for providing template and example code.

---