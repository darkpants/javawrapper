/*
 * Copyright (c) 2019 Hugo Donkerbroek
 * All rights reserved
 */
package nl.bioinf.hadonkerbroek.readmissionreader;


import weka.classifiers.meta.Stacking;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.IOException;

/**
 * This class uses the machine learning model to predict new cases given in a file.
 *
 * @author hugo
 */
public class WekaRunner {
    private final String modelFile = "stackingmodel.model";

    public void start(String unknownArffFile) {
        try {
            Instances instances = loadArff(unknownArffFile);
            Stacking fromFile = loadClassifier();
            classifyNewInstance(fromFile, instances);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(Stacking model, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = model.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private Stacking loadClassifier() throws Exception {
        return (Stacking) weka.core.SerializationHelper.read(modelFile);
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}
