/*
 * Copyright (c) 2019 Hugo Donkerbroek
 * All rights reserved
 */
package nl.bioinf.hadonkerbroek.readmissionreader;


import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 */
public final class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
            }

        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
