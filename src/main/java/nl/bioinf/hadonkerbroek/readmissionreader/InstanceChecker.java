/*
 * Copyright (c) 2019 Hugo Donkerbroek
 * All rights reserved
 */
package nl.bioinf.hadonkerbroek.readmissionreader;


import java.util.Arrays;

/**
 * This class processes a single instance given in the command line.
 *
 * @author hugo
 */
public class InstanceChecker {
    public String singleInstanceChecker(String instance) {
        String[] attributes = instance.split(",");
        if (attributes.length == 11) {
            for (int i = 0; i < attributes.length; i++) {
                if (attributes[i].equals("")) {
                    attributes[i] = "0";
                    System.out.println("Attribute value missing, replaced with: 0");
                }
            }
        } else if (attributes.length < 11) {
            System.out.println("Not enough attributes given. (string of 11 values expected)");
            return "Error";
        } else if (attributes.length > 11) {
            System.out.println("Too many attributes given. (string of 11 values expected)");
            return "Error";
        } else {
            System.out.println("An unexpected error occurred.");
            return "Error";
        }
        //removing '[' and ']' so only the instance remains
        String x = Arrays.toString(attributes);
        String[] y = x.split("(\\[|])");
        return y[1];
    }
}
