/*
 * Copyright (c) 2019 Hugo Donkerbroek
 * All rights reserved
 */
package nl.bioinf.hadonkerbroek.readmissionreader;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author hugo
 */
public class ApacheCliOptionsProvider {
    private static final String HELP = "help";
    private static final String FILE = "file";
    private static final String INSTANCE = "instance";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option fileOption = new Option("f", FILE, true, "a .arff file");
        Option instOption = new Option("i", INSTANCE, true, "a single instance");

        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(instOption);
    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (commandLine.hasOption(FILE)) {
                String fileStr = this.commandLine.getOptionValue(FILE);
                WekaRunner runner = new WekaRunner();
                runner.start(fileStr);
            }

            else if (commandLine.hasOption(INSTANCE)) {
                String instance = this.commandLine.getOptionValue(INSTANCE);
                InstanceChecker checker = new InstanceChecker();
                String cleanInstance = checker.singleInstanceChecker(instance);
                if (cleanInstance != "Error") {
                    TmpFileProcessor tmp = new TmpFileProcessor();
                    //create a temporary file tmp
                    tmp.fileCreator();
                    //append single instance to the tmp file
                    String singleInstanceFile = tmp.appendToFile(cleanInstance);
                    //run model on tmp file
                    WekaRunner runner = new WekaRunner();
                    runner.start(singleInstanceFile);
                    //remove tmp file
                    tmp.removeFile();
                } else {
                    System.out.println("Program stopping... -h for help");
                }
            }

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("yes", options);
    }
}
