/*
 * Copyright (c) 2019 Hugo Donkerbroek
 * All rights reserved
 */
package nl.bioinf.hadonkerbroek.readmissionreader;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This class creates a temporary file which is used to process a single instance given in the commandline.
 *
 * @author hugo
 */
public class TmpFileProcessor {

    public void fileCreator() {
        //create a file
        try {
            String fileSeparator = System.getProperty("file.separator");

            //relative path
            String relativePath = "tmp" + fileSeparator + "tmp.arff";
            File file = new File(relativePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String appendToFile(String instance) {
        try {
            //get .arff file template
            String template = Files.readString(Paths.get("template.arff"), StandardCharsets.US_ASCII);
            //append the template to the empty file
            BufferedWriter writer = new BufferedWriter(new FileWriter("tmp/tmp.arff", true));
            writer.write(template);
            //append the single instance
            writer.write(instance);
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "tmp/tmp.arff";
    }

    public void removeFile() {
        //delete the temp file
        try {
            Files.deleteIfExists(Paths.get("tmp/tmp.arff"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
